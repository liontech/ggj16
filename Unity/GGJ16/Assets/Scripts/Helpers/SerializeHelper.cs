﻿using System.Xml.Serialization;
using System.IO;

public static class SerializeHelper {

    public static void Serialize<T>(string path, T content) where T : class {
        var serializer = new XmlSerializer(typeof(T));
        var stream = new FileStream(path, FileMode.Create);
        serializer.Serialize(stream, content);
        stream.Close();
    }

    public static T Deserialize<T>(string path) where T : class {
        var serializer = new XmlSerializer(typeof(T));
        var stream = new FileStream(path, FileMode.Open);
        T instance = serializer.Deserialize(stream) as T;
        stream.Close();
        return instance;
    }

}