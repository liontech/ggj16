﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class CustomEase : ScriptableObject {
    public AnimationCurve Curve;
}
