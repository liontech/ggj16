﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class ReflectionHelper {

	public static List<Type> GetAllSubclasses(this Type type) {
        Type[] allTypes = type.Assembly.GetTypes();
        List<Type> subclassTypes = new List<Type>();
        foreach (Type t in allTypes) {
            if (!t.IsSubclassOf(type)) { continue; }
            subclassTypes.Add(t);
        }
        return subclassTypes;
    }

    public static List<Type> GetAllImplementations(this Type interfaceType) {
        Type[] allTypes = interfaceType.Assembly.GetTypes();
        List<Type> implementingTypes = new List<Type>();
        foreach (Type t in allTypes) {
            if (t == interfaceType) { continue; }
            if (!interfaceType.IsAssignableFrom(t)) { continue; }
            implementingTypes.Add(t);
        }
        return implementingTypes;
    }

}
