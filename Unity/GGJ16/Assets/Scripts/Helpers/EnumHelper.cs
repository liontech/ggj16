﻿

using System;
using System.Collections.Generic;

public class EnumHelper {
    
    public static T[] GetValues<T>() {
        Array array = Enum.GetValues(typeof(T));
        T[] typedArray = new T[array.Length];
        for (int i = 0; i < array.Length; i++) {
            typedArray[i] = (T)array.GetValue(i);
        }
        return typedArray;
    }

}
