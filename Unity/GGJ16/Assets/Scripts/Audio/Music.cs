﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Audio;

public class Music : MonoBehaviour {

    [Serializable]
    private class MusicAudioSource {
        public string ID = null;
        public AudioSource Source = null;
    }
    
    static Music instance;
    
    [SerializeField] MusicAudioSource[] musicSources;

    float musicVolumeOnStart;

    public static void PlayMusic(string id, float startVolume = .0f) {
        foreach (MusicAudioSource musicSource in instance.musicSources) {
            if (musicSource.ID == id) {
                Audio.Instance.PlayLoop(musicSource.Source, 
                                        startVolume, 
                                        musicSource.ID);
            } else {
                Audio.Instance.StopLoop(musicSource.ID);
            }
        }
    }

    public static void StopMusic() {
        foreach (MusicAudioSource musicSource in instance.musicSources) {
            Audio.Instance.StopLoop(musicSource.ID);
        }
    }

    private void Awake() {
        instance = this;
    }

}
