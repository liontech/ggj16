﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Audio : MonoBehaviour {

    public static Audio Instance {
        get {
            if (instance == null) {
                GameObject go = new GameObject("AudioManager");
                instance = go.AddComponent<Audio>();
            }
            return instance;
        }
    }

    private const float FADE_DURATION = .5f;

    private static Audio instance;

    private static Dictionary<string, AudioSource> loopingSounds = new Dictionary<string, AudioSource>();
    private static Dictionary<string, AudioSource> fadingSounds = new Dictionary<string, AudioSource>();

    public AudioSource PlaySound(AudioSource original) {
        if (original == null) { return null; }
        AudioSource source = gameObject.InstantiateAsChild<AudioSource>(original.gameObject);
        StartCoroutine(DestroyOnFinish(source));
        return source;
    }

    public AudioSource PlayLoop(AudioSource original, float startVolume = 1.0f, string id = null) {
        if (original == null) { return null; }
        if (id != null) {
            if (loopingSounds.ContainsKey(id)) {
                return loopingSounds[id];
            }
            if (fadingSounds.ContainsKey(id)) {
                loopingSounds.Add(id, fadingSounds[id]);
                fadingSounds.Remove(id);
                return loopingSounds[id];
            }
        }

        AudioSource source = gameObject.InstantiateAsChild<AudioSource>(original.gameObject);
        source.loop = true;
        source.volume = startVolume;
        if (id != null) {
            loopingSounds.Add(id, source);
        }
        return source;
    }

    public void StopLoop(string id, bool instant = false) {
        if (!loopingSounds.ContainsKey(id)) { return; }
        AudioSource audioSource = loopingSounds[id];
        loopingSounds.Remove(id);

        if (!instant) {
            fadingSounds.Add(id, audioSource);
            return;
        }

        audioSource.Stop();
        Destroy(audioSource.gameObject);
    }

    private IEnumerator DestroyOnFinish(AudioSource source) {
        while (source.isPlaying) {
            yield return new WaitForEndOfFrame();
        }
        Destroy(source.gameObject);
    }

    private void Update() {
        foreach (string id in new List<string>(fadingSounds.Keys)) {
            fadingSounds[id].volume -= Time.deltaTime / FADE_DURATION;
            if (fadingSounds[id].volume <= 0.0f) {
                Destroy(fadingSounds[id].gameObject);
                fadingSounds.Remove(id);
            }
        }

        foreach (KeyValuePair<string, AudioSource> pair in loopingSounds) {
            pair.Value.volume = Mathf.Clamp01(pair.Value.volume + Time.deltaTime / FADE_DURATION);
            
        }
    }

}
