﻿using UnityEngine;
using System.Collections;

public class ShakeEffect : MonoBehaviour {

    private const float SHAKE_INTERVAL = 0.02f;

    private float amplitude = .3f;

    private Vector3 startPosition;
    private float timeSinceLastShake;

    private float shakeDurationleft;
    private bool mustConstantShake;

    public void Shake(float power, bool additive = false) {
        if (additive) {
            shakeDurationleft += power;
            return;
        }
        shakeDurationleft = power;
    }

    public void SetConstantShake(float power) {
        mustConstantShake = true;
        shakeDurationleft = power;
    }

    private void Awake() {
        startPosition = transform.localPosition;
        timeSinceLastShake = 0.0f;
    }

    private void Update() {
        if (mustConstantShake || shakeDurationleft > 0.0f) {
            timeSinceLastShake += Time.deltaTime;
            if (timeSinceLastShake > SHAKE_INTERVAL) {
                timeSinceLastShake = 0.0f;
                transform.localPosition = startPosition + GetRandomShakeOffset();
            }

            if (shakeDurationleft > 0.0f) {
                shakeDurationleft -= Time.deltaTime;
                if (shakeDurationleft <= 0) {
                    shakeDurationleft = 0.0f;
                    transform.localPosition = startPosition;
                }
            }
        }
    }

    private Vector3 GetRandomShakeOffset() {
        Vector3 maxOffset = new Vector3(Random.Range(-amplitude, amplitude),
                                        Random.Range(-amplitude, amplitude),
                                        Random.Range(-amplitude, amplitude));
        return maxOffset * shakeDurationleft;
    }

}
