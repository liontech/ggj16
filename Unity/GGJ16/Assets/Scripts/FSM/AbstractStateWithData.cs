﻿using UnityEngine;
using System.Collections;
using System;

public abstract class AbstractStateWithData<T> : AbstractState where T : AbstractStateData {
    
    protected T stateData { get; private set; }

    public void SetData(T data) {
        stateData = data;
    }

}
