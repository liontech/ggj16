﻿using UnityEngine;
using System.Collections;

public abstract class AbstractState : MonoBehaviour {

    protected AbstractFSM fsm { get; private set; }
    protected float timeInState { get; private set; }

    private bool isActive;

    public void Initialize(AbstractFSM fsm) {
        this.fsm = fsm;
    }

	public void Activate() {
        timeInState = 0.0f;
        isActive = true;
        OnActivate();
    }

    public void Deactivate() {
        isActive = false;
        OnDeactivate();
    }

    protected abstract void OnActivate();
    protected abstract void OnDeactivate();
    protected abstract void OnActiveUpdate();

    protected void Update() {
        if (isActive) {
            timeInState += Time.deltaTime;
            OnActiveUpdate();
        }
    }

}
