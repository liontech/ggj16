﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class AbstractFSM : AbstractState {

    [SerializeField] private bool showLog;

    private List<AbstractState> states = new List<AbstractState>();

    private AbstractState currentState;

    public void GotoState<T>() where T : AbstractState {
        if (currentState != null) {
            currentState.Deactivate();
        }
        currentState = GetOrCreateState<T>();

        LogGotoStateIfNeeded<T>();

        currentState.Activate();
    }

    public void GotoState<T, U>(U data)
        where T : AbstractStateWithData<U>
        where U : AbstractStateData {
        if (currentState != null) {
            currentState.Deactivate();
        }
        AbstractStateWithData<U> state = GetOrCreateState<T>();
        state.SetData(data);
        currentState = state;

        LogGotoStateIfNeeded<T>();

        state.Activate();
    }

    private T GetOrCreateState<T>() where T : AbstractState {
        AbstractState state = states.Find(x => x.GetType() == typeof(T));
        if (state == null) {
            state = gameObject.GetComponent<T>();
            if (state == null) {
                state = gameObject.AddComponent<T>();
            }
            state.Initialize(this);
            states.Add(state);
        }
        return state as T;
    }

    private void LogGotoStateIfNeeded<T>() where T : AbstractState {
        if (showLog) {
            Debug.Log("Entering '" + typeof(T).Name + "'");
        }
    }

    protected override void OnActivate() { }
    protected override void OnDeactivate() { }
    protected override void OnActiveUpdate() { }

}
